import java.util.Scanner;

public class BoardGameApp{
	public static void main(String[] args){
		System.out.println("WELCOME TO SIMPLE WAR GAME");
		Scanner reader = new Scanner(System.in);
		Board game = new Board(5); //initializes a board of 5x5
		int numCastles = 7;
		int amountTurns = 8;
		int gameTurn = 1; //turn counter (for display purposes)
		
		
		while (amountTurns != 0 && numCastles != 0) {
			System.out.println("----------------------------------------------------");
			System.out.println("TURN " + gameTurn + ":");
			System.out.println("Number of Castles Left: " + numCastles);
			
			System.out.println(game);
			System.out.println("Column you'd like to place your token in:"); // ask user input
			int cols = Integer.parseInt(reader.nextLine());
			System.out.println("Row you'd like to place your token in:"); // second user input
			int rows = Integer.parseInt(reader.nextLine());
			System.out.println();
			
			int result = game.placeToken(rows-1, cols-1); // calls tbe function and store value in variable
			if (result < 0) {
				boolean resolved = false;
				while (!resolved) { // makes user go in a loop until they have made a proper input that allows them to continue playing
					System.out.println("Invalid Input! TRY AGAIN! \n");
					System.out.println("Column you'd like to place your token in:");
					int cols2 = Integer.parseInt(reader.nextLine());
					System.out.println("Row you'd like to place your token in:");
					int rows2 = Integer.parseInt(reader.nextLine());
					System.out.println();
					
					result = game.placeToken(rows2-1, cols2-1); //replaces old values with newer values
					if (result >= 0) { //if result is not negative meaning the user has chosen a new spot on the board that has not been occupied
						resolved = true; //causes loop to end
					}
				}
			}
			else if (result == 1) {
				System.out.println("There was a WALL!! \n");
			}
			else {
				System.out.println("CASTLE placed. \n");
				numCastles--;
			}
				
			System.out.println("Changes:");
			System.out.println(game);
			
			amountTurns--; // reduces the amount of turns left
			gameTurn++; //increments turns past
		}
		
		if (numCastles == 0){ //checks if user has used all their pieces when they run out of turns
			
			System.out.println("---------------------------------------------------- \n YOU WON! \n----------------------------------------------------");
		}
		else { //when user still has pieces
			System.out.println("---------------------------------------------------- \n GAME OVER! \n----------------------------------------------------");
		}
	}
}